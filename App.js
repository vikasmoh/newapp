import React from 'react';
import MainStack from './screens/navigation/stacknavigtaion';
import { NavigationContainer } from '@react-navigation/native';
// import Tabs from './screens/navigation/tab';
import { MenuProvider } from 'react-native-popup-menu';
const App=()=>{
  return(
    <MenuProvider>
   <NavigationContainer>
     <MainStack>
       {/* <tabs/> */}
     </MainStack>
   </NavigationContainer>
   </MenuProvider>
    )
}
export default App;
