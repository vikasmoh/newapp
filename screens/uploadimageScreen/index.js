import React from 'react';
import {View,Text,Image,StyleSheet, TouchableOpacity} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, heightPercentageToDP, widthPercentageToDP } from '../../utility/index';
// import imageScreen from '../imageScreen';
const uploadimageScreen=({navigation})=>{
return(
    <View style={styles.mainview}>
      {/* <View style={{margin:widthPercentageToDP('1%')}} */}
        <View style={styles.vw2}>
            <Text style={styles.txt0}>Profile Image</Text>
        </View>
        <View style={styles.imgvw}>
            <Image style={styles.img} source={require('../../assets/images/pckr2.png')}/>
        </View>
        <View style={styles.bor1}>
            <Text style={styles.btn1}>Upload Image</Text>
        </View>
        <View style={styles.bor2}>
        <Text style={styles.btn2}>Next</Text>
        </View>
        <View style={styles.drct}>
        <TouchableOpacity onPress={()=>navigation.navigate('image')}>
        <View style={styles.vew00}>
            <Text style={styles.txt01}>Skip Now</Text>
            <Image style={styles.icon} source={require('../../assets/images/next.png')}/>
        </View>
        </TouchableOpacity>
        </View>
    </View>
)
};
const styles=StyleSheet.create({
    mainview:{
        alignItems:'center',
    justifyContent:'center',
    marginTop:0,
    flex:1,
    backgroundColor:'#e5eff4',
    
    // margintop:100
    
   },
   imgvw:{
    marginBottom:20
   },
   txt0:{
    marginBottom:20,
    fontSize:33,
    color:'#333333',
    fontFamily:'Poppins-Light',
    // fontWeight:'bold'

    // fontWeight:'700'
   },
   vw2:{
    // marginTop:20
   },
    img:{
        width:widthPercentageToDP('50%'),
        height:heightPercentageToDP('22%'),

        // marginBottom:40
    },
    bor1:{
        borderRadius: 50,
        margin:'5%',
        padding:15,
        width:'75%',
        borderWidth:1,
        alignItems: "center",
        backgroundColor:'#ffffff',
        borderColor:'#0A2339'
    },
    btn1:{
        color:'#0A2339',
        fontSize:20,
        fontFamily:'Poppins-Regular'
    },
    bor2:{
        backgroundColor:'#0A2339',
        borderRadius: 50,
        // borderWidth:1,
        alignItems: "center",
      width:"75%",
    //   marginLeft:'5%',
      padding:15,
        borderColor:'#0A2339'
    },
    btn2:{
        color:'#ffffff',
        fontSize:20,
        fontFamily:'Poppins-Regular'
    },
    vew00:{
        top:20
    },
    txt01:{
        fontSize:18,
        fontFamily:'Poppins-Regular'
    },
    icon:{
        height:hp(1.8),
        width:wp(2.05),
        left:95,
        bottom:26
        
    },
    drct:{
        flexDirection:'row',
        justifyContent:'center'
    }   
    

})
export default uploadimageScreen;