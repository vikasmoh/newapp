// import { Picker } from "@react-native-picker/picker";
import React, { useEffect, useState } from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity, ScrollView, FlatList,Alert, Modal } from 'react-native';
import { Input, Icon } from 'react-native-elements';
import { ActivityIndicator } from "react-native-paper";
// import DrawerContent from "../navigation/DrawerContent";
// import { Rating, AirbnbRating } from 'react-native-ratings';
import Menu, {
    MenuContext,
    MenuTrigger,
    MenuOptions,
    MenuOption,
    renderers
  } from 'react-native-popup-menu';
// import ModalDropdown from 'react-native-modal-dropdown';
import { widthPercentageToDP as wp, heightPercentageToDP as hp} from "../../utility";
const Alisting = ({navigation}) => {
    const [setStyle1, setStyles] = useState('');
    const [isModalVisible, setModalVisible] = useState(false);
    const [hrt,setHrt]=useState(false);
    const toggleModal = () => {
        setModalVisible(!isModalVisible);
      };
     
    const onClick = (name) => {
        setStyles(name)
    }
    const [newArray, setNewArray] = useState([
        { image: require('../../assets/images/iikn.png'), item: 'House for sale', price: '$15,00,000 - $18,00,000' ,status:'false'},
    ])
    useEffect(()=>{
    },[hrt]);
    const ratingCompleted = () => {
        console.log("Done Rating");
    }
    const setheart=(item)=>{
        setHrt(!hrt);
        console.log("Vka ,",item);
        if(item["status"]==="true"){
            console.log("Vikas.......")
            var newItem=item["status"]="false";
        }else{
            var newItem=item["status"]="true";
        }  
        console.log("newItemmm with heart",newArray);
    }
    const newrender = ({ item, index }) => {
        return (
            <View>
                <View style={{ margin: 20,padding:5,backgroundColor: '#E5E5E5',width:wp('90%'),borderTopLeftRadius:20,borderTopRightRadius:20 }}>
                
                <View style={{position:'absolute',zIndex:1,alignSelf:'flex-end',top:10,right:5}}>
                <TouchableOpacity style={{}} onPress={()=>setheart(item)
                }>
                {item.status=='true'?
                    <Image style={{resizeMode:'center'}} source={require('../../assets/images/redhrt.png')}>
                    </Image>:
                    <Image style={{resizeMode:'center'}} source={require('../../assets/images/blkhrt.png')}></Image>}
                    </TouchableOpacity>
                    </View>
                    <Image style={{height:hp('28%'),width:wp('87%'),borderTopLeftRadius:20,borderTopRightRadius:20,marginBottom:10}}source={item.image}></Image>
                    <Text style={{fontSize:20,fontWeight:'400',color:'#333333',opacity:0.7,fontFamily:'Poppins-Regular'}}>{item.item}</Text>
                    <View>
                        <Text style={{fontSize:20,color:'#0A2339',fontFamily:'Poppins-Medium',fontWeight:'500',opacity:0.9}}>{item.price}</Text>
                    </View>
                    <View style={{ flexDirection: 'row',marginTop:10 }}>
                        <View>
                            <Image style={{height:20,width:15}} source={require('../../assets/images/lcn2.png')}>
                            </Image>
                        </View>
                        <View>
                            <Text style={{left:5,fontSize:16,color:'#555555',fontFamily:'Poppins-Regular',fontWeight:'400',opacity:0.7}}>Moqadishu, Somalia</Text>
                        </View>
                    </View>
                    <View style={{borderBottomWidth:2,margin:10,borderBottomColor:'#D3D6D8',opacity:0.8}}>
                    <View style={{ flexDirection: 'row',justifyContent:'space-between',marginBottom:10}}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{right:5}}>
                                <Image source={require('../../assets/images/noptn.png')} style={{ height: 22, width: 22 }}></Image>
                            </View>
                            <View>
                                <Text style={{fontSize:17,color:'#333333',fontFamily:'Poppins-Regular',fontWeight:'600'}}>Property</Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'row',}}>
                            <View>
                                <Image style={{ height: 20, width: 20 }} source={require('../../assets/images/nwdbl.png')}></Image>
                            </View>
                            <View>
                                <Text style={{fontSize:17,color:'#333333',fontFamily:'Poppins-Regular',fontWeight:'600'}}>300 &times; 200(m)</Text>
                            </View>
                        </View>
                        
                    </View>
                    </View>
                    <View style={{ flexDirection: 'row',justifyContent:'space-between'}}>
                        <View>
                            <Image style={{height:hp('5%'), width:wp('12%'),resizeMode:'center'}} source={require('../../assets/images/lg.png')}></Image>
                        </View>
                        <View style={{}}>
                            <View>
                                <Text style={{fontSize:16,color:'#004566',fontFamily:'Poppins-Regular',fontWeight:'400',opacity:0.8,right:25}}>Ahmed Sayed</Text>
                            </View>
                            <View style={{right:27,}}>
                            <View style={{flexDirection:'row',justifyContent:'space-evenly'}}>
                                <View>
                                    <Image source={require('../../assets/images/start.png')}></Image>
                                </View>
                                <View>
                                <Image source={require('../../assets/images/start.png')}></Image>
                                    </View>
                                    <View>
                                    <Image source={require('../../assets/images/start.png')}></Image>
                                    </View>
                                    <View>
                                    <Image source={require('../../assets/images/start.png')}></Image>
                                    </View>
                                    <View>
                                    <Image source={require('../../assets/images/start.png')}></Image>
                                    </View>
                            </View>
                            
                                {/* <Rating
                                
                                   // style={{backgroundColor:'red'}}
                                    ratingBackgroundColor='transparent'
                                    tintColor="#E5E5E5"
                                    
                                    type='star'
                                    ratingCount={5}
                                    imageSize={20}
                                    //   showRating
                                    onFinishRating={ratingCompleted}
                                /> */}
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{bottom:9,right:14}}>
                                <Image style={{width:wp('5%'),height:hp('5%'),resizeMode:'center'}} source={require('../../assets/images/nclndr.png')}></Image>
                            </View>
                            <View style={{}}>
                                <Text style={{fontSize:16,color:'#555555',fontFamily:'Poppins-Regular',fontWeight:'400',opacity:0.8,right:10,top:4}}>5 days ago</Text>
                            </View>
                        </View>
                    </View>
                    
                </View>
            </View>
        )
    }
    const keyExtractor = (item, index) => item + index;

    const newrender1=({item,index})=>{
        return(
        <View>
            {/* <View style={{flexDirection:'row',width:'90%',borderWidth:1}}> */}
            <View style={{ margin: 20,padding:5,backgroundColor: '#E5E5E5',width:wp('93%'),
            borderTopLeftRadius:20,flexDirection:'row',height:hp('27%'),borderBottomLeftRadius:20 }}>
             <View style={{position:'absolute',zIndex:1,alignSelf:'flex-end',top:10,right:'60%'}}>
                <TouchableOpacity style={{}} onPress={()=>setheart(item)
                }>
                {item.status=='true'?
                    <Image style={{resizeMode:'center'}} source={require('../../assets/images/redhrt.png')}>
                    </Image>:
                    <Image style={{resizeMode:'center'}} source={require('../../assets/images/blkhrt.png')}></Image>}
                    </TouchableOpacity>
                    </View>
                <View style={{width:wp('40%')}}>
                   <Image  source={item.image} style={{height:hp('26%'),width:wp('40%'),borderBottomLeftRadius:20,borderTopLeftRadius:20,marginBottom:10}}></Image>
                </View>
                <View style={{justifyContent:'center'}}>
                <View style={{marginLeft:20}}>
                    <Text style={{fontSize:20,fontWeight:'400',color:'#333333',opacity:0.7,fontFamily:'Poppins-Regular'}}>{item.items}</Text>
                    <Text style={{fontSize:20,color:'#0A2339',fontFamily:'Poppins-Medium',fontWeight:'500',opacity:0.9}}>{item.price}</Text>
            <View style={{flexDirection:'row',marginTop:10}}>
                <View>
                <Image style={{height:20,width:15}} source={require('../../assets/images/lcn2.png')}></Image>
                </View>
                <View>
                <Text style={{left:5,fontSize:16,color:'#555555',fontFamily:'Poppins-Regular',fontWeight:'400',opacity:0.7}}>Moqadishu, Somalia</Text>
             </View>
             </View>
             <View style={{ flexDirection: 'row',margin:10 }}>
                            <View style={{right:5}}>
                                <Image source={require('../../assets/images/noptn.png')} style={{ height: 22, width: 22 }}></Image>
                            </View>
                            <View>
                                <Text style={{fontSize:17,color:'#333333',fontFamily:'Poppins-Regular',fontWeight:'600'}}>Property</Text>
                            </View>
                        </View>
             <View style={{flexDirection:'row',marginTop:10}}>
                            <View>
                                <Image style={{ height: 20, width: 20 }} source={require('../../assets/images/nwdbl.png')}></Image>
                            </View>
                            <View>
                                <Text style={{fontSize:17,color:'#333333',fontFamily:'Poppins-Regular',fontWeight:'600'}}>300 &times; 200(m)</Text>
                            </View>
                        </View>
                        </View>
                </View>
                </View>
                
              
        </View>
        )
    }

    const keyExtractor1=(item,index)=>item + index;

    // const newDatarender=({item,index})=>{
    // //    alert("hello");
    // // <View style={{margin:'20%'}}>
    // //     <Text>Vikas</Text>
    // // </View>
    // }
    // const [loading, setLoading] = useState(false);

    // const startLoading = () => {
    //   setLoading(true);
    //   setTimeout(() => {
    //     setLoading(false);
    //   }, 3000);
    // };

    return (
        <View style={styles.container}>
        
            <ScrollView nestedScrollEnabled={true}>
                <View style={styles.mainvw}>
                    <Text style={styles.txt0}>Location</Text>
                    <Image style={styles.img0} resizeMethod="auto" source={require('../../assets/images/dwnarw.png')} />
                </View>
                <View style={styles.vw1}>
                
                 
                 <Menu >
                    <MenuTrigger>
                   
                    <Image style={styles.img1}  source={require('../../assets/images/fnt.png')}></Image>
                   
                    </MenuTrigger>
                
      <MenuOptions  optionsContainerStyle={styles.menuOptions}>
        <MenuOption onSelect={() => alert(`English`)} text='English'  customStyles={{optionWrapper: { padding: 10}, optionText: styles.text001}} />
        <MenuOption onSelect={() => alert(`Somali`)} text='Somali'  customStyles={{optionWrapper: { padding: 10}, optionText: styles.text002}} >
          {/* <Text style={{color: 'blue'}}>Delete</Text> */}
        </MenuOption>
        {/* <MenuOption onSelect={() => alert(`Not called`)} disabled={true} text='Disabled' /> */}
      </MenuOptions>
    </Menu>
                 
                        <TouchableOpacity>
                    <Image style={styles.img2} source={require('../../assets/images/bell.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                    <Image style={styles.img3} source={require('../../assets/images/bar.png')} />
                    </TouchableOpacity>
                </View>
                <View style={styles.vw2}>
                    <Image style={styles.img4} source={require('../../assets/images/location.png')} />
                    <Text style={styles.txt1}>Moqadishu, Somalia</Text>
                </View>
                <View style={styles.v7}>
                    <View style={styles.vw3}>
                        <Input
                        inputContainerStyle={{borderBottomWidth:0}}
                            style={styles.inputText1}
                            placeholder='Search'
                            // underlineColorAndroid="white"
                            leftIcon={
                                <Icon
                                    name='search'
                                    size={24}
                                    color='black'
                                />
                            }
                        />
                    </View>
                    <View style={styles.vw4}>
                    <TouchableOpacity onPress={()=>navigation.navigate('fltrs')}>
                        <Image style={styles.img5} source={require('../../assets/images/sldr.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.imageview}>
                {/* <ActivityIndicator size='large' color='white'> */}
                    <TouchableOpacity onPress={() => onClick('store')} >
                    {/* <TouchableOpacity onPress={()=>navigation.navigate('Alist')}> */}
                        <View style={setStyle1 == 'store' ? styles.vikdiv : styles.div1}>
                            <Image style={styles.image1} source={require('../../assets/images/home.png')} />
                            <Text style={styles.tx1}>Store</Text>
                        </View>
                        </TouchableOpacity>
                    {/* </TouchableOpacity> */}
                    <TouchableOpacity onPress={() => onClick('property')}>
                    {/* <TouchableOpacity onPress={()=>navigation.navigate('Alist')}> */}
                        <View style={setStyle1 == 'property' ? styles.vikdiv2 : styles.div2}>
                            <Image style={styles.image2} source={require('../../assets/images/building.png')} />
                            <Text style={styles.tx2}>Property</Text>
                        </View>
                        </TouchableOpacity>
                    {/* </TouchableOpacity> */}
                    <TouchableOpacity onPress={() => onClick('land')}>
                    {/* <TouchableOpacity onPress={()=>navigation.navigate('Alist')}> */}
                        <View style={setStyle1 == 'land' ? styles.vikdiv3 : styles.div3}>
                            <Image style={styles.image3} source={require('../../assets/images/land.png')} />
                            <Text style={styles.tx3}>Land</Text>
                        </View>
                        </TouchableOpacity>
                    {/* </TouchableOpacity> */}
                    {/* </ActivityIndicator> */}
                </View>
                <View style={styles.vw5}>
                    <Text style={styles.tx4}>AllListings</Text>
                </View>
                
                <View>
                    <FlatList
                        data={newArray}
                        renderItem={newrender}
                        keyExtractor={keyExtractor}
                        // nestedScrollEnabled={true}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        // numColumns={newArray.length}
                    >
                    </FlatList>
                </View>
                <View>
                <FlatList
                        data={newArray}
                        renderItem={newrender}
                        keyExtractor={keyExtractor}
                        // nestedScrollEnabled={true}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                    // numColumns={newArray.length}
                    >
                    </FlatList>
                </View>
                <View>
                <FlatList
                        data={newArray}
                        renderItem={newrender}
                        keyExtractor={keyExtractor}
                        // nestedScrollEnabled={true}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                    // numColumns={newArray.length}
                    >
                    </FlatList>
                </View>
                <View>
                    {/* <Text>Chacha</Text> */}
                </View>
            </ScrollView>
            
        </View>
    )
};
const styles = StyleSheet.create({
    container: {
        // marginTop:20,
        backgroundColor: '#fffff'
    },
    mainvw: {
        marginLeft:'5%',
        flexDirection: 'row',
        // margin:10
        // margintop:20
    },
    inputText1: {
        // borderBottomWidth:'none'
        // borderWidth:1
    },
    txt0: {
        fontFamily: 'Poppins-Regular',
        fontSize: 17,
        // left:20
        // color:'#555555'
    },
    img0: {
        marginLeft:5,
        width: wp('3%'),
        height: hp('1%'),
        alignSelf:'center'
    },
    text001:{
        fontSize:20,
        color:'#333333',
        fontFamily:'Poppins-Regular'
    },
    text002:{
        fontSize:20,
        color:'#333333',
        fontFamily:'Poppins-Regular',
        
    },
    vw1: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        // justifyContent:'space-evenly',
        // width:'30%'

    },
    img1: {
        height:25,width:30,
        // width: wp('7%'),
        // height: hp('3%'),
        margin: 5
    },
    img2: {
        height:30,width:25,
        // width: wp('5%'),
        // height: hp('3%'),
        margin: 5
    },
    img3: {
        height:20,width:20,
        // width: wp('6%'),
        // height: hp('2%'),
        margin: 9
    },
    vw2: {
        top:-20,
        flexDirection: 'row',
        // marginRight: 15
    },
    img4: {
        resizeMode:'center',
        // bottom: 1,
       
        left:13
    },
    txt1: {
        fontSize: 20,
        color: '#333333',
        fontFamily: 'Poppins-Regular',
        left: 20
    },
    input: {
        fontSize: 20,
        marginLeft: 25
        // backgroundColor:'#f3f3f3'
    },
    menuOptions: {
        
        shadowColor: 'black',
        shadowOpacity: 0.3,
        backgroundColor:'#d3d6d8',
        shadowOffset: { width: 0, height: 0 },
        borderRadius: 10,
        width:wp('30%'),
        height:hp('13%')
},
    vw3: {
        width: wp('75%'),
        height: hp('6%'),
        backgroundColor: '#e5e5e5',
        borderRadius: 5
    },
    v7: {
        // backgroundColor:'green',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginTop:10,
        

    },
    vw4: {
        backgroundColor: '#0A2339',
        // borderWidth:1,
        borderRadius: 5,
        width: wp('15%'),
        height: hp('6%'),
    },
    img5: {
        // width:wp('4%'),
        // height:hp('6%'),
        resizeMode: 'center',
        height: 50,
        width: 50,
        alignSelf: 'center'
    },
    imageview: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        margin: 10
        // alignSelf:'center',
        // marginTop:30

    },
    div1: {
        
        height: hp("12%"),
        width:wp("30%"),
        borderRadius: 17,
        backgroundColor: '#e5e5e5',
       
    },
    div2: {
        height: hp("12%"),
        width:wp("30%"),
        backgroundColor: '#e5e5e5',
        // margin:10,
        borderRadius: 17,
        // justifyContent:'space-between'
    },
    div3: {
        height: hp("12%"),
        width:wp("30%"),
        borderRadius: 17,
        backgroundColor: '#e5e5e5',
      },
    image1: {
        resizeMode: 'center',
        height: hp("5%"),
        width: wp('8%'),
        alignSelf: 'center',
        marginTop:25
        // alignItems:'center'
},
    image2: {
        // alignSelf:'center',
        resizeMode: 'center',
        height: hp("5%"),
        width: wp('8%'),
        alignSelf: 'center',
        marginTop:25
    },
    image3: {
        // alignSelf:'center',
        resizeMode: 'center',
        height: hp("11%"),
        width: wp('25%'),
        alignSelf: 'center',
        
    },
    tx1: {
        textAlign: 'center',
        color: '#333333',
        fontSize: 20,
        fontFamily: 'Poppins-Regular',
        // bottom: 20
    },
    tx2: {
        textAlign: 'center',
        color: '#333333',
        fontSize: 20,
        fontFamily: 'Poppins-Regular',
        // bottom: 20
    },
    tx3: {
        textAlign: 'center',
        color: '#333333',
        fontSize: 20,
        fontFamily: 'Poppins-Regular',
        bottom: 25
    },
    vikdiv: {
        // borderWidth: 1,
        height: hp("12%"),
        width:wp("30%"),
        borderRadius: 17,
        borderColor:'#0A2339',
        borderWidth: 1,
        // backgroundColor: '#D4F0F5',
    },
    vikdiv2: {
        // borderWidth: 1,
        height: hp("12%"),
        width:wp("30%"),
        borderColor:'#0A2339',
        borderWidth: 1,
        borderRadius: 17,
    },
    vikdiv3: {
        // borderWidth: 1,
        height: hp("12%"),
        borderRadius: 17,
        width:wp('30%'),
        borderColor:'#0A2339',
        borderWidth: 1,
},
    vw5: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 18
    },
    tx4: {
        color: '#333333',
        fontSize: 23,
        fontFamily: 'Poppins-Regular',
        // fontWeight:'600'
    },
    tx5: {
        // color: 'green',
        fontSize: 20,
        top: 7,
        fontFamily: 'Poppins-Regular',
        color:'#004566',
        left:16
    },
    vw5:{
    
    },
    tx4: {
        color: '#333333',
        fontSize: 23,
        fontFamily: 'Poppins-Regular',
        // marginLeft:30
        left:'8%',
        top:'19%'
    },
})
export default Alisting;