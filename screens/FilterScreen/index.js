import React,{useState} from 'react';
import { Text,View,Image, TouchableOpacity,Alert,StyleSheet, TextInput, ScrollView, FlatList} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp} from "../../utility";
// import MultiRangeSlider from "multi-range-slider-react";
import Slider from '@react-native-community/slider';

import { CheckBox } from 'react-native-elements/dist/checkbox/CheckBox';
import { Checkbox } from 'react-native-paper';
// import { View } from 'react-native-animatable';

// import { View } from 'react-native-animatable';
const FilterScreen=({navigation})=>{
    const [chnages,setChanges]=useState(false);
    const [setStyle1, setStyles] = useState('store');
    // const [setStyle1, setStyles] = useState('third');
    const [userdata,setUserData] = useState([{"Amenities":'Air Conditioning',"status":false},{"Amenities":'Barbeque',"status":false},{"Amenities":'Dryer',"status":false},
    {"Amenities":'Gym',"status":false},{"Amenities":"Laundry","status":false},{"Amenities":"Lawn","status":false},
    {"Amenities":"Microwave","status":false},{"Amenities":"Outdoor Shower","status":false},{"Amenities":"Refrigerator",
    "status":false},{"Amenities":"Sauna","status":false},{"Amenities":"Swimming Pool","status":false},{"Amenities":"TV Cable",
  "status":false},{"Amenities":"washer","status":false},{"Amenities":"WiFi","status":false},{"Amenities":"Window coverings","status":false}])
    const onClick = (name) => {
        setStyles(name)
    }
    const [setStyle2,setStyle21]=useState('');
    const onClicks=(name)=>{
        setStyle21(name)
    }
    const [minValue, set_minValue] = useState(25);
const [maxValue, set_maxValue] = useState(75);
const handleInput = (e) => {
	set_minValue(e.minValue);
	set_maxValue(e.maxValue);
};
const setCHcek=(item)=>{
    item["status"]=!item["status"];
    setChanges(!chnages);
}
const [dropDown,setDropDown]=useState(false);
const [menuData,setMenuData]=useState('Listing:Newest to oldest');
const renderItem=({item,index})=>{
    return(
    <View>
                
            <View style={{flexDirection:'row',margin:2,}}>
            {/* <Text>hello</Text> */}
            <TouchableOpacity onPress={()=>setCHcek(item)}>
                <View style={item.status?{width:25,height:25,backgroundColor:'#0A2339',borderRadius:5,marginLeft:10,alignItems:'center',borderColor:"#BFBEBE"}:
                {width:25,height:25,backgroundColor:'#BFBEBE',borderRadius:2,marginLeft:10,alignItems:'center'}}>
                {item.status?
                    <Image source={require('../../assets/images/chcek.png')} style={{alignSelf:'center',marginTop:9}}></Image>:null}
                </View>
                </TouchableOpacity>
        <View style={{marginLeft:10}}>
            <Text style={{fontSize:16,fontWeight:"400",fontFamily:'Poppins-Regular',color:'#333333'}}>{item.Amenities}</Text>
        </View>
        </View>
        
        </View>
    )
}
return(
    <ScrollView style={{backgroundColor:"#fff",flex:1}} >
    {/* <View style={{backgroundColor:"green"}}> */}
    <View>
     <View>
     <TouchableOpacity onPress={()=>navigation.navigate('location')}>
      <Image style={{top:46,left:9}} source ={require('../../assets/images/back.png')}/>
      </TouchableOpacity>
        <View style={{flexDirection:'row',justifyContent:'space-between',margin:25}}>
           <Text style={{color:'#333333',fontFamily:'Poppins-Regular',fontSize:18,fontWeight:'500'}}>Filter</Text>
           <View style={{width:wp('19%'), height:hp('4%'),justifyContent:'center',backgroundColor:'#F3F3F3',borderRadius:8}}>
               <Text style={{fontSize:14,fontWeight:'400',fontFamily:'Poppins-Regular',color:'#0A2339',textAlign:'center'}}>Reset</Text>
           </View>
         </View>
         <Text style={{fontSize:16,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',marginLeft:10}}>Sortby</Text>
         <View style={{width:wp('90%'), height:hp('5%'),alignSelf:'center',
         backgroundColor:'#F3F3F3',borderRadius:5,flexDirection:'row',justifyContent:'space-between'}}>
             <Text style={{fontSize:14,left:15,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',
             textAlign:'center',marginTop:10}}>{menuData}</Text>
             <TouchableOpacity onPress={()=>setDropDown(!dropDown)}>
             <Image style={{marginTop:18,right:10}} source={require('../../assets/images/dwnarw.png')}/>
             </TouchableOpacity>
         </View>
         {dropDown?
         <View style={{width:wp('90%'), height:hp('23%'),
         backgroundColor:'#F3F3F3',borderRadius:5,marginTop:5,alignSelf:'center'}}>
            <TouchableOpacity onPress={()=>setMenuData('Listing:Newest to oldest')}>
            <Text style={{fontSize:14,left:15,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',
            marginTop:10,lineHeight:38}}>Listing:Newest to oldest</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>setMenuData('Listing: Oldest to newest')}>
              <Text style={{fontSize:14,left:15,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',
             marginTop:10,lineHeight:38}}>Listing: Oldest to newest</Text>
             </TouchableOpacity>
             <TouchableOpacity onPress={()=>setMenuData('Low to high Price')}>
              <Text style={{fontSize:14,left:15,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',
            marginTop:10,lineHeight:38}}>Price: Low to high Price</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>setMenuData('Price: High to low')}>
              <Text style={{fontSize:14,left:15,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',
             marginTop:10,lineHeight:38}}>Price: High to low</Text>
             </TouchableOpacity>
            
         </View>:null
         }
         <View style={{marginTop:25}}>
             <Text style={{fontSize:16,fontWeight:'400',fontFamily:'Poppins-Regular',color:'#333333',marginLeft:10}}>Sort Category</Text>
             <View style={styles.imageview}>
                    {/* <TouchableOpacity onPress={() => onClick('store')} > */}
                    <TouchableOpacity onPress={()=>setStyles('store')}>
                        <View style={setStyle1 == 'store' ? styles.vikdiv : styles.div1}>
                            <Image style={styles.image1} source={require('../../assets/images/home.png')} />
                            <Text style={styles.tx1}>Store</Text>
                        </View>
                        </TouchableOpacity>
                    {/* </TouchableOpacity> */}
                    {/* <TouchableOpacity onPress={() => onClick('pprty')}> */}
                    <TouchableOpacity onPress={()=>setStyles('pprty')}>
            
                        <View style={setStyle1 == 'pprty' ? styles.vikdiv2 : styles.div2}>
                            <Image style={styles.image2} source={require('../../assets/images/building.png')} />
                            <Text style={styles.tx2}>Property</Text>
                        </View>
                        {/* </TouchableOpacity> */}
                    </TouchableOpacity>
                    {/* <TouchableOpacity onPress={() => onClick('third')}> */}
                    <TouchableOpacity onPress={()=>setStyles('third')}>
                        <View style={setStyle1 == 'third' ? styles.vikdiv3 : styles.div3}>
                            <Image style={styles.image3} source={require('../../assets/images/land.png')} />
                            <Text style={styles.tx3}>Land</Text>
                        </View>
                        </TouchableOpacity>
                    {/* </TouchableOpacity> */}
                </View>
         </View>
         <View style={{marginTop:15}}>
         <Text style={{fontSize:16,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',left:10}}>Location</Text>
         <TextInput style={{width:wp('90%'), height:hp('5%'),alignSelf:'center',backgroundColor:'#F3F3F3',borderRadius:5,fontSize:14,fontWeight:'400',color:'#555555'}}
             placeholder='Enter Location'
             
         />
         {(setStyle1=="store") || ( setStyle1== "pprty")?
         <View>
         <Text style={{fontSize:16,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',margin:10}}>I would like to</Text>
         <View style={{flexDirection:'row',}}>
            <View style={{borderWidth:1,padding:10,borderRadius:10,width:'25%',alignItems:'center',marginLeft:10,backgroundColor:'#0A2339'}}>
             <Text style={{fontSize:14,fontWeight:'400',color:'#ffff'}}>Buy</Text>
             </View>
             <View style={{padding:10,borderRadius:10,width:'25%',alignItems:'center',marginLeft:30,
             backgroundColor:'#F3F3F3',opacity:0.5}}>
             <Text style={{fontSize:14,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular'}}>Rent</Text>
             </View>
             
         </View>
         </View>:null}
         <View style={{margin:6}}>
                 <Text style={{fontSize:16,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',lineHeight:24}}>Price Range ($)</Text>
        </View>
      
         <View style={{flexDirection:'row',}}>
            <View style={{padding:10,borderRadius:5,width:80,alignItems:'center',height:40,marginLeft:10, backgroundColor:'#F3F3F3'}}>
             <Text>5,000</Text>
             </View>
             <View>
             <Slider
  style={{width: 200, height: 40}}
  minimumValue={0}
  maximumValue={1}
  minimumTrackTintColor="#FFFFFF"
  maximumTrackTintColor="#000000"
/>
             </View>
             <View style={{padding:10,height:40,borderRadius:5,width:80,alignItems:'center',marginLeft:30, backgroundColor:'#F3F3F3'}}>
             <Text>85,000</Text>
             </View>
         </View>
         <View style={{margin:6}}>
                 <Text style={{fontSize:16,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',lineHeight:24}}>Area Length (m)</Text>
        </View>
        <View style={{flexDirection:'row',}}>
            <View style={{padding:10,borderRadius:5,width:80,alignItems:'center',height:40,marginLeft:10, backgroundColor:'#F3F3F3'}}>
             <Text>5,000</Text>
             </View>
             <View>
             <Slider
  style={{width: 200, height: 40}}
  minimumValue={0}
  maximumValue={1}
  minimumTrackTintColor="#FFFFFF"
  maximumTrackTintColor="#000000"
/>
             </View>
             <View style={{padding:10,height:40,borderRadius:5,width:80,alignItems:'center',marginLeft:30, backgroundColor:'#F3F3F3'}}>
             <Text>8,600</Text>
             </View>
         </View>
         <View style={{margin:6}}>
                 <Text style={{fontSize:16,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',lineHeight:24}}>Area Width (m)</Text>
        </View>
        <View style={{flexDirection:'row',}}>
            <View style={{padding:10,borderRadius:5,width:80,alignItems:'center',height:40,marginLeft:10, backgroundColor:'#F3F3F3'}}>
             <Text>5,000</Text>
             </View>
             <View>
             <Slider
  style={{width: 200, height: 40}}
  minimumValue={0}
  maximumValue={1}
  minimumTrackTintColor="#FFFFFF"
  maximumTrackTintColor="#000000"
/>
             </View>
             <View style={{padding:10,height:40,borderRadius:5,width:80,alignItems:'center',marginLeft:30, backgroundColor:'#F3F3F3'}}>
             <Text>8,600</Text>
             </View>
         </View>
         <View style={{margin:6}}>
                 <Text style={{fontSize:16,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',lineHeight:24}}>Seller Rating</Text>
        </View>
         </View>
         {/* <View style={{flexDirection:'row',justifyContent:'space-evenly'}}> */}
         <View style={styles.mnveu}>
          <TouchableOpacity onPress={()=>onClicks('block1')}>
          {/* <View style={setStyle1=='store' ? styles.vikdiv:styles.div1}>   */}

             <View style={setStyle2=='block1'? styles.vkdv:styles.dva}> 
                <Image style={styles.imga} source={require('../../assets/images/start.png')}/>
                 <Text>1</Text>
             </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>onClicks('block2')}>
             <View style={setStyle2=='block2'? styles.vkdv1:styles.dvb}> 
                <Image style={styles.imgb} source={require('../../assets/images/start.png')}/>
                 <Text>2</Text>
             </View>
             </TouchableOpacity>
             <TouchableOpacity onPress={()=>onClicks('block3')}>
             <View style={setStyle2=='block3'? styles.vkdv2:styles.dvc}> 
                <Image style={styles.imgc} source={require('../../assets/images/start.png')}/>
                 <Text>3</Text>
             </View>
             </TouchableOpacity>
             <TouchableOpacity onPress={()=>onClicks('block4')}>
             <View style={setStyle2=='block4'? styles.vkdv3:styles.dvd}> 
                <Image style={styles.imgd} source={require('../../assets/images/start.png')}/>
                 <Text>4</Text>
             </View>
             </TouchableOpacity>
             <TouchableOpacity onclick={()=>onClicks('block5')}>
             <View style={setStyle2=='block5'? styles.vkdv4:styles.dve}> 
                <Image style={styles.imge} source={require('../../assets/images/start.png')}/>
                 <Text>5</Text>
             </View>
             </TouchableOpacity>
         </View>
         <View style={{margin:6}}>
                 <Text style={{fontSize:16,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',lineHeight:24}}>Seller</Text>
                 <TextInput style={{width:wp('90%'), height:hp('5%'),alignSelf:'center',backgroundColor:'#F3F3F3',borderRadius:5,fontSize:14,fontWeight:'400',color:'#555555',marginTop:10}}
             placeholder='Enter Seller Name'
             
             
         />
        </View>
        <View style={{margin:6}}>
                 <Text style={{fontSize:16,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',lineHeight:24}}>Post Time</Text>
                 <View style={{width:wp('90%'), height:hp('5%'),alignSelf:'center',marginTop:10,
         backgroundColor:'#F3F3F3',borderRadius:5,flexDirection:'row',justifyContent:'space-between'}}>
             <Text style={{fontSize:14,left:15,fontWeight:'400',color:'#333333',fontFamily:'Poppins-Regular',
             textAlign:'center',marginTop:10}}>Select time</Text>
             <TouchableOpacity onPress={()=>Alert.alert('menubar')}>
             <Image style={{marginTop:18,right:10}} source={require('../../assets/images/dwnarw.png')}/>
             </TouchableOpacity>
         </View>
        </View>
        {(setStyle1=="pprty") ?
        <View style={{margin:6,borderBottomWidth:1,width:wp('95%'),borderColor:'#777777',}}>
         <Text style={{fontFamily:'Poppins-Regular',fontSize:16,fontWeight:'500',color:'#333333',lineHeight:24}}>Amenities</Text>
    </View>:null}
 
     </View>

     {setStyle1=="pprty"?

     <View>
         <FlatList
             data={userdata}
             renderItem={renderItem}
            //  keyExtractor={}
         />
     </View>:null}
     <TouchableOpacity onPress={()=>navigation.navigate('Alist')}>
     <View style={{borderWidth:1,borderRadius:30,width:220,height:45,justifyContent:'center',alignSelf:'center',backgroundColor:'#0A2339',margin:20}}>
            <Text style={{fontSize:16,fontWeight:'400',fontFamily:'Poppins-Regular',textAlign:'center',color:'#FFFFFF',}}>Apply & Search</Text>
        </View>
    </TouchableOpacity>
    </View>
    </ScrollView>
)
};
const styles=StyleSheet.create({
    mnveu:{
        flexDirection:'row',
        justifyContent:'space-evenly'
    },
    dva:{
        // borderWidth:1,
        flexDirection:'row',
        width:50,
        height:40,
        alignItems:'center'
        ,justifyContent:'center',
        borderRadius:5,
        backgroundColor:'#F3F3F3'
    },
    dvb:{
        // borderWidth:1,
        flexDirection:'row',
        width:50,
        height:40,
        alignItems:'center'
        ,justifyContent:'center',
        borderRadius:5,
        backgroundColor:'#F3F3F3'
    },
    dvc:{
        // borderWidth:1,
        flexDirection:'row',
        width:50,
        height:40,
        alignItems:'center'
        ,justifyContent:'center',
        borderRadius:5,
        backgroundColor:'#F3F3F3'
    },
    dvd:{
        // borderWidth:1,
        flexDirection:'row',
        width:50,
        height:40,
        alignItems:'center'
        ,justifyContent:'center',
        borderRadius:5,
        backgroundColor:'#F3F3F3'
    },
    dve:{
        // borderWidth:1,
        flexDirection:'row',
        width:50,
        height:40,
        alignItems:'center'
        ,justifyContent:'center',
        borderRadius:5,
        backgroundColor:'#F3F3F3'
    },
    imga:{
        marginRight:4
    },
    imgb:{
        marginRight:4
    },
    imgc:{
        marginRight:4
    },
    imgd:{
        marginRight:4
    },
    imge:{
        marginRight:4
    },
    imageview: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        margin: 10
    },
    vkdv:{
        flexDirection:'row',
        borderWidth:1,
        width:50,
        height:40,
        alignItems:'center'
        ,justifyContent:'center',
        borderRadius:5,
        backgroundColor:'#F3F3F3'
    },
    vkdv1:{
        borderWidth:1,
        flexDirection:'row',
        width:50,
        height:40,
        alignItems:'center'
        ,justifyContent:'center',
        borderRadius:5,
        backgroundColor:'#F3F3F3'
    },
    vkdv2:{
        borderWidth:1,
        flexDirection:'row',
        width:50,
        height:40,
        alignItems:'center'
        ,justifyContent:'center',
        borderRadius:5,
        backgroundColor:'#F3F3F3'
    },
    vkdv3:{
        borderWidth:1,
        flexDirection:'row',
        width:50,
        height:40,
        alignItems:'center'
        ,justifyContent:'center',
        borderRadius:5,
        backgroundColor:'#F3F3F3'
    },
    vkdv4:{
        borderWidth:1,
        flexDirection:'row',
        width:50,
        height:40,
        alignItems:'center'
        ,justifyContent:'center',
        borderRadius:5,
        backgroundColor:'red'
    },
    vikdiv: {
        height: hp("12%"),
        width:wp("30%"),
        borderRadius: 17,
        borderWidth:1,
        borderColor:'#0A2339'
        // backgroundColor: '#D4F0F5',
    },
    vikdiv2: {
        borderWidth: 1,
        height: hp("12%"),
        width:wp("30%"),
        borderWidth:1,
        borderRadius: 17,
        borderColor:'#0A2339'
    },
    vikdiv3: {
        borderWidth:1,
        borderColor:'#0A2339',
        borderWidth: 1,
        height: hp("12%"),
        borderRadius: 17,
        width:wp('30%'),
        
},
div1: {
        
    height: hp("12%"),
    width:wp("30%"),
    borderRadius: 17,
    backgroundColor: '#F3F3F3',
   
},
div2: {
    height: hp("12%"),
    width:wp("30%"),
    backgroundColor: '#F3F3F3',
    // margin:10,
    borderRadius: 17,
    // justifyContent:'space-between'
},
div3: {
    height: hp("12%"),
    width:wp("30%"),
    borderRadius: 17,
    backgroundColor: '#F3F3F3',
  },
  image1: {
    resizeMode: 'center',
    height: hp("5%"),
    width: wp('8%'),
    alignSelf: 'center',
    marginTop:25
    // alignItems:'center'
},
image2: {
    // alignSelf:'center',
    resizeMode: 'center',
    height: hp("5%"),
    width: wp('8%'),
    alignSelf: 'center',
    marginTop:25
},
image3: {
    // alignSelf:'center',
    resizeMode: 'center',
    height: hp("11%"),
    width: wp('25%'),
    alignSelf: 'center',
    
},
tx1: {
    textAlign: 'center',
    color: '#333333',
    fontSize: 20,
    fontFamily: 'Poppins-Regular',
    // bottom: 20
},
tx2: {
    textAlign: 'center',
    color: '#333333',
    fontSize: 20,
    fontFamily: 'Poppins-Regular',
    // bottom: 20
},
tx3: {
    textAlign: 'center',
    color: '#333333',
    fontSize: 20,
    fontFamily: 'Poppins-Regular',
    bottom: 25
},
})
export default FilterScreen;
