import React, { useState } from "react";
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, heightPercentageToDP, widthPercentageToDP }
    from "../../utility";
import Tabs from "../navigation/tab";
const categoryScreen = ({ navigation }) => {
    const [setStyle1, setStyles] = useState('');
    const onClick = (name) => {
        setStyles(name)
    }
    return (
        <View style={styles.container}>
            <View>
                <Image />
            </View>
            <View style={styles.cont2}>
                <Image style={styles.img} source={require('../../assets/images/icon.png')} />
            </View>
            <View style={styles.txt}>
                <Text style={styles.txt1}>Select Category</Text>
                <Text style={styles.txt2}>Select atleast one category to continue</Text>
            </View>
            <View style={styles.imageview}>
                <TouchableOpacity onPress={() => onClick('store')}>
                    {/* <View style={setStyle1=='store'? styles.vikdiv:styles.div1}>   */}
                    <View style={setStyle1 == 'store' ? styles.vikdiv : styles.div1}>

                        <Image style={styles.image1} source={require('../../assets/images/home.png')} />
                        <Text style={styles.tx1}>Store</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => onClick('property')}>
                    <View style={setStyle1 == 'property' ? styles.vikdiv2 : styles.div2}>
                        <Image style={styles.image2} source={require('../../assets/images/building.png')} />
                        <Text style={styles.tx2}>Property</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => onClick('land')}>
                    <View style={setStyle1 == 'land' ? styles.vikdiv3 : styles.div3}>
                        <Image style={styles.image3} source={require('../../assets/images/land.png')} />
                        <Text style={styles.tx3}>Land</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={styles.bor2}>
                <Text style={styles.btn2}>Submit</Text>
            </View>
            <View style={styles.drct}>
                <View style={styles.bor3}>
                    <TouchableOpacity onPress={() => navigation.navigate('location')}>
                        <Text style={styles.btn3}>Skip Now</Text>
                        <Image style={styles.icon} source={require('../../assets/images/next.png')} />

                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e5eff4',

    },
    cont2: {
        marginTop: 36
    },
    img: {
        // width: wp("69%"),
        height: hp("22%"),
        alignSelf: 'center',
        resizeMode: 'center'
    },
    txt: {
        marginTop: 5,
        // alignSelf:'center',
        alignItems: 'center'

    },
    txt1: {
        marginTop: 20,
        fontSize: 34,
        // fontWeight:'bold',
        color: '#333333',
        fontFamily: 'Poppins-Medium',
        opacity: 0.9
    },
    txt2: {
        fontSize: 18,
        fontWeight: '100',
        color: '#777777',
        fontFamily: 'Poppins-Medium',
        // opacity:0.8
        marginBottom: 40
    },
    imageview: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: 10
        // alignItems:'center',
        // alignSelf:'center'
    },
    div1: {
        // borderWidth:1,
        height: hp("17%"),
        width: wp("30%"),
        borderRadius: 17,
        backgroundColor: '#D4F0F5',
        // borderWidth:1
        // margin:10,
        // justifyContent:'space-between'
    },

    vikdiv: {
        borderWidth: 1,
        height: hp("17%"),
        width: wp("30%"),
        borderRadius: 17,
        backgroundColor: '#D4F0F5',
    },
    div2: {
        // borderWidth:1,
        height: hp("17%"),
        width: wp("30%"),
        backgroundColor: '#D4F0F5',
        // margin:10,
        borderRadius: 17,
        // justifyContent:'space-between'
    },
    vikdiv2: {
        borderWidth: 1,
        height: hp("17%"),
        width: wp("30%"),
        backgroundColor: '#D4F0F5',
        // margin:10,
        borderRadius: 17,
    },
    div3: {
        // borderWidth:1,
        height: hp("17%"),
        borderRadius: 17,
        backgroundColor: '#D4F0F5',
        // margin:10,
        width: wp("30%"),
        // justifyContent:'space-between'
    },
    vikdiv3: {
        borderWidth: 1,
        height: hp("17%"),
        borderRadius: 17,
        backgroundColor: '#D4F0F5',
        // margin:10,
        width: wp("30%"),
        // justifyContent:'space-between'
    },
    tx1: {
        textAlign: 'center',
        color: '#333333',
        fontSize: 20,
        fontFamily: 'Poppins-Regular'
    },
    tx2: {
        textAlign: 'center',
        color: '#333333',
        fontSize: 20,
        fontFamily: 'Poppins-Regular'
    },
    tx3: {
        textAlign: 'center',
        color: '#333333',
        fontSize: 20,
        fontFamily: 'Poppins-Regular'
    },
    image1: {
        resizeMode: 'center',
        height: hp("11%"),
        width: wp('25%'),
        alignSelf: 'center'
        // width:wp("6%"),
    },
    image2: {
        alignSelf: 'center',
        resizeMode: 'center',

        height: hp("11%"),
        width: wp('25%'),
    },
    image3: {
        alignSelf: 'center',
        resizeMode: 'center',
        height: hp("11%"),
        width: wp('25%'),
    },
    btn2: {

        color: '#FFFFFF',
        fontSize: 20,
        fontFamily: 'Poppins-Regular'

    },
    bor2: {
        margin: 30,
        backgroundColor: '#0A2339',
        borderRadius: 50,
        // borderWidth:1,
        alignItems: "center",
        alignSelf: 'center',
        //   width:"90%",
        //   marginLeft:'4%',
        //   padding:15,
        justifyContent: 'center',
        borderColor: '#0A2339',
        width: widthPercentageToDP('79%'),
        height: heightPercentageToDP('8%'),

    },
    bor3: {
        margin: 20,
        // flexDirection:'column'
    },
    btn3: {

        fontSize: 20,
        color: '#333333',
        textAlign: 'center',
        fontFamily: 'Poppins-Regular',
    },
    icon: {
        height: hp(1.8),
        width: wp(2.05),
        left: 95,
        bottom: 26

    },
    drct: {
        flexDirection: 'row',
        justifyContent: 'center'
    }
})
export default categoryScreen;