import React, { useEffect } from 'react';
import {View,Image, StyleSheet,Text} from 'react-native';
const Splash =({navigation})=>{
  useEffect(()=>{
     timeOutHandle=setTimeout(()=>{
       nextScreen()
     },2000)
  },[]);
  const nextScreen=()=>{
    navigation.navigate('selectLanguage')
  }
  return(
    <View style={styles.container}>
    <Image style={styles.img} resizeMode='center' source={require('../../../assets/images/maskgroup.png')}/>
    {/* <Image
        style={styles.img}
        resizeMode='center'
        source={require('../../../assets/images/icon.png')}
      /> */}
      {/* <Text style={styles.txt}>HOY IYO DEEGAAN</Text> */}
    </View>
  )
}
const styles=StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#0A2339',
    // alignItems:'center',
    justifyContent:'center',
  },
  img:{
   width:'100%',
   height:'70%',
   alignSelf:'center'
  },
// txt:{
//   color:"#ffffff",
//   // bottom:20,
//   alignSelf:'center',
//   fontSize:16,
//   top:-32,
//   fontFamily:'Poppins-Bold'
// }
  
})
export default Splash;