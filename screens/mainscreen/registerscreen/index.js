import React,{useState,useRef} from 'react';
import {View,Text,Image,StyleSheet, TouchableOpacity, TouchableOpacityBase,Alert,Modal, Button, TextInput, ScrollView} from 'react-native';
// import PhoneInput from 'react-native-phone-number-input';
const Register=({navigation})=>{
//   const [phoneNumber, setPhoneNumber] = useState('');
//   const phoneInput = useRef(null);
// const [showModal1,setShowModal1]=useState(false);
const [modalVisible, setModalVisible] = useState(false);
const [modalVisible1,setModalVisible1]=useState(false);
    return(
      <ScrollView>
        <View style={styles.container}>
            
            <View style={{marginTop:'10%',alignSelf:'center'}}>
                <Image style={styles.appicon} source={require('../../../assets/images/appicon.png')} resizeMode="contain"></Image>
            </View>
            <View style={{marginBottom:'20%'}}>
            <View style={{alignSelf:'center',margin:'2%'}}>
            {/* <View style={{alignSelf:'center'}}>
                <Text style={{fontSize:33,color:'#333333',fontFamily:'Poppins-Regular',fontWeight:'600'}}>Welcome Back!</Text>
                <Text style={{alignSelf:'center',fontSize:20,fontFamily:'Poppins-Regular',}}>Login to continue</Text>
            </View> */}
                <Text style={{fontSize:33,color:'#333333',fontFamily:'Poppins-Regular',color:'#333333',fontWeight:'600'}}>Create Account</Text>
                <Text style={{alignSelf:'center',fontFamily:'Poppins-Regular',fontSize:20,fontFamily:'Poppins-Regular',color:'#777777'}}>Register to continue</Text>
            </View>
            <View style={styles.inside}>
              <TextInput style={styles.TextInput}
                placeholder='Enter name'
              />
            </View> 
            <View style={styles.inside}>
              <TextInput style={styles.TextInput}
                placeholder='Enter phone number'
              />
            </View> 
            
            
            <TouchableOpacity onPress={()=>navigation.navigate('verificationScreen')}>
            <View style={{backgroundColor:'#0A2339',width:'90%',padding:15,borderRadius:30,marginLeft:'6%',height:60}}>
                    <Text style={{color:'white',alignSelf:'center',fontSize:20,fontFamily:'Poppins-Regular'}}>Register</Text>
                </View>
                </TouchableOpacity>
            <View style={{alignItems:'center',margin:'2%'}}>
                <Text style={{fontSize:18,marginTop:25,color:"#777777",fontFamily:'Poppins-Regular'}}>Or Register with</Text>
            </View>
                <View style={{flexDirection:'row',margin:'3%',justifyContent:'space-evenly'}}>
                <View style={{borderColor:'#D3D6D8',width:'50%',right:5,padding:10,borderRadius:50,borderWidth:1,backgroundColor:'white',flexDirection:'row',justifyContent:'space-evenly'}}>
                <Image source={require('../../../assets/images/google.png')} style={{alignSelf:'center',height:20,width:20}}></Image>
                    <Text style={{alignSelf:'center',fontSize:20,fontFamily:'Poppins-Regular'}}>Google</Text>
                </View>
                <View style={{width:'50%',padding:10,left:5,borderRadius:50,borderWidth:1,borderColor:'#D3D6D8',backgroundColor:'white',flexDirection:'row',justifyContent:'space-evenly'}}>
                <Image source={require('../../../assets/images/facebook.png')} style={{alignSelf:'center'}}></Image>
                    <Text style={{alignSelf:"center",fontSize:20,fontFamily:'Poppins-Regular'}}> Facebook</Text>
                </View>
                </View>
                <View style={{margin:'5%'}}>
                    <Text style={styles.text1}>By singing up you accept the</Text>
                    <View style={{flexDirection:'row',margin:'5%',alignSelf:'center'}}>
                    <View>
                    <TouchableOpacity onPress={()=>setModalVisible(true)}>
                        <Text style={styles.txtclr}>Terms & Conditions </Text>
                        </TouchableOpacity>
                        </View>
                        <View>
                       
                        <Text style={styles.txt01}>and </Text>
                       
                        </View>
                        <View>
                        <TouchableOpacity onPress={()=>setModalVisible1(true)}>
                        <Text style={styles.txtclr1}>Privacy Policies</Text>
                        </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{flexDirection:'row',alignSelf:'center'}}>
                
                    <Text style={styles.txt}>Already have an account?</Text>
                    <TouchableOpacity onPress={()=>navigation.navigate('accountScreen')}>
                    <Text style={{fontSize:18,fontFamily:'Poppins-Regular',fontWeight:'700',color:'#0A2339',alignSelf:'center',color:'#487b92'}}> Login</Text>
                    </TouchableOpacity>
                </View>
                </View>
                <View>
                <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View  style={{backgroundColor:'white',height:'90%',width:'90%',borderRadius:20,margin:'10%',alignSelf:'center'}}>
          <View style={{alignSelf:'center'}}>
            <Text>Modal Text Here</Text>
           
          </View>
          <View style={{backgroundColor:'red',padding:10,borderRadius:20}}>
          <TouchableOpacity onPress={()=>setModalVisible(false)}>
              <Text>I understand</Text>
              </TouchableOpacity>
          </View>
        </View>
      </Modal>
                </View>
                <View>
                <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible1}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View  style={{backgroundColor:'white',height:'90%',width:'90%',borderRadius:20,margin:'10%',alignSelf:'center'}}>
          <View style={{alignSelf:'center'}}>
            <Text>Modal Text Here</Text>
           
          </View>
          <View style={{backgroundColor:'red',padding:10,borderRadius:20}}>
          <TouchableOpacity onPress={()=>setModalVisible1(false)}>
              <Text>I understand</Text>
              </TouchableOpacity>
          </View>
        </View>
      </Modal>
                </View>
        </View>
         </ScrollView>
    )
};
const styles=StyleSheet.create({
    container:{
        backgroundColor:'#e5eff4',
        flex:1,
        alignContent:'center',
        justifyContent:'center',
        
    },
    appicon:{
     height:200,
     width:200,
      //  resizeMode:'center',
    //  width:'20%',
    //  height:'20%',
    //   top:40
      
      
  },
    phoneNumberView: {
        width: '80%',
        height: 50,
      },
      txt:{
          fontFamily:'',
          color:'#555555'
      },
      inside:
      {
        backgroundColor:'#ffffff',
      // borderWidth:1,
      borderColor:'#D3D6D8',
       borderRadius:30,
       width: "90%",
       height: 60,
       marginBottom: 20,
      alignSelf:'center'
      },
      TextInput:{
    
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
    fontSize:18,
    fontFamily:'Poppins-Regular'
      },
      text1:{
        textAlign:'center',
        color:'#555555',
        fontSize:18,
        fontFamily:'Poppins-Regular'
        
      },
      txt01:{
      color:'#555555',
      fontSize:18,
      fontFamily:'Poppins-Regular'
      },
      txtclr:{
        color:'#004566',
        fontSize:18,
        fontFamily:'Poppins-Regular'
      },
      txtclr1:{
        color:
          '#004566',
          fontSize:18
      },
      txt:{
        fontSize:18,
        fontFamily:'Poppins-Regular'
      }
})
export default Register;
