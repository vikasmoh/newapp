import React,{useState,useRef} from 'react';
import {View,Text,Image,StyleSheet, TouchableOpacity, TouchableOpacityBase} from 'react-native';
import PhoneInput from 'react-native-phone-number-input';
const loginScreen=({navigation})=>{
  const [phoneNumber, setPhoneNumber] = useState('');
  const phoneInput = useRef(null);
    return(
        <View style={styles.container}>
            <View>
                <Image>
                </Image>
            </View>
            <View style={{marginTop:'10%',alignSelf:'center'}}>
                <Image style={styles.appicon} source={require('../../../assets/images/appicon.png')} resizeMode="contain"></Image>
            </View>
            <View>
            <View style={{alignSelf:'center'}}>
                <Text style={{fontSize:33,color:'#333333',fontFamily:'Poppins-Regular',fontWeight:'600'}}>Welcome Back!</Text>
                <Text style={{alignSelf:'center',fontSize:20,fontFamily:'Poppins-Regular',}}>Login to continue</Text>
            </View>
            <View style={{margin:'6%',alignItems:'center',backgroundColor:'white',borderRadius:20}}>
      <PhoneInput
        ref={phoneInput}
        defaultValue={phoneNumber}
        // defaultCode="IN"
        // layout="first"
        // withShadow
        // autoFocus
        // containerStyle={styles.phoneNumberView}
        textContainerStyle={{ paddingVertical: 2}}
        
        onChangeFormattedText={text => {
          setPhoneNumber(text);
        }}
      />
            </View>
            <TouchableOpacity onPress={()=>navigation.navigate('verificationScreen')}>
            <View style={{backgroundColor:'#0A2339',width:'90%',padding:13,borderRadius:50,marginLeft:'6%'}}>
                    <Text style={{color:'white',alignSelf:'center',fontSize:20,fontFamily:'Poppins-Regular'}}>Login</Text>
                </View>
                </TouchableOpacity>
            <View style={{alignItems:'center',margin:'2%'}}>
                <Text style={{fontSize:18,fontFamily:'Poppins-Regular',top:14}}>Or login with</Text>
            </View>
                <View style={{flexDirection:'row',margin:'6%',justifyContent:'space-evenly'}}>
                <View style={{borderColor:'#D3D6D8',width:'45%',padding:15,borderRadius:50,borderWidth:1,backgroundColor:'white',flexDirection:'row',justifyContent:'space-evenly'}}>
                <Image source={require('../../../assets/images/gglicon.png')} style={{height:22,width:21,alignSelf:'center'}}></Image>
                    <Text style={{alignSelf:'center',fontSize:20,color:'#0A2339'}}>Google</Text>
                </View>
                <View style={{width:'45%',padding:15,borderRadius:50,borderWidth:1,borderColor:'#D3D6D8',backgroundColor:'white',flexDirection:'row',justifyContent:'space-evenly'}}>
                <Image source={require('../../../assets/images/fcb.png')} style={{alignSelf:'center',height:23,width:20}}></Image>
                    <Text style={{alignSelf:"center",fontSize:20,color:'#0A2339'}}> Facebook</Text>
                </View>
                </View>
                <View style={{flexDirection:'row',alignSelf:'center'}}>
                
                    <Text style={styles.txt}>Don’t have an account?</Text>
                    {/* <TouchableOpacity onPress={()=>navigation.navigate('register')}> */}
                    <TouchableOpacity onPress={()=>navigation.navigate('verificationScreen')}>
                    <Text style={{fontSize:16,fontWeight:'700',color:'#0A2339',alignSelf:'center',color:'#487b92',
                    alignSelf:'center',fontSize:20,fontFamily:'Poppins-Regular',}}> Register</Text>
                    </TouchableOpacity>
                </View>
                </View> 
        </View>
    )
};
const styles=StyleSheet.create({
    container:{
        backgroundColor:'#e5eff4',
        flex:1,
        
    },
    appicon:{
        height:200,width:200,
        // height:'45%',
        // resizeMode:'center',
        // height:'35%',
        top:20,
        
    },
    phoneNumberView: {
        width: '80%',
        height: 50,
        // fontSize:18,
        // fontFamily:'Poppins-Regular'
      },
      txt:{
        alignSelf:'center',
        fontSize:20,
        fontFamily:'Poppins-Regular',
      }
})
export default loginScreen;
