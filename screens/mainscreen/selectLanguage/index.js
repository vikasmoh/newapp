import React from 'react';
import {View,Image,Text,StyleSheet, TouchableOpacity} from 'react-native';

const Language=({navigation})=>{
    
    return(
        <View style={styles.container}>
        <View style={styles.cont2}>
            <Image style={styles.img} source = {require('../../../assets/images/icon.png')} resizeMode='contain'/> 
        </View>
          <View style={styles.txt}>
             <Text style={styles.txt1}>Select Language</Text>
              <Text style={styles.txt2}>Please select language to continue</Text>
          </View>

          <View style={styles.plc}>
         
         <View style={styles.bor1}>
            <Text style={styles.btn1}>English</Text>
        </View>
    
            <View style={styles.bor2}>
            <TouchableOpacity onPress={()=>navigation.navigate('OnboardingScreen')}>
            <Text style={styles.btn2}>Somali</Text>
            </TouchableOpacity>
         </View> 
         </View>
        </View>
    )
}
const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#e8f1f5',
    },
    cont2:{
        marginTop:'25%' 
    },
    img:{
        alignSelf:'center',
        height:180,
        width:180
    },
    txt1:{
        marginTop:20,
        textAlign:'center',
        fontFamily:'Poppins-Light',
        fontSize:42,
        fontWeight:'500',
        color:'#333333',
        
        // marginBottom:10,
    },
    txt2:{
      fontSize:25,
      fontWeight:'100'  ,
      fontFamily:'Poppins',
      color:'#777777'
    },
    txt:{
       alignSelf:'center',
       marginBottom:20
    },
 
bor1:{
    borderRadius: 50,
    margin:'5%',
    padding:15,
    width:'90%',
    borderWidth:1,
    alignItems: "center",
    backgroundColor:'#0A2339'
},
bor2:{
    borderRadius: 50,
    borderWidth:1,
    alignItems: "center",
  width:"90%",
  marginLeft:'5%',
  padding:15,
    borderColor:'#0A2339'
},

    btn1:{
        color:'#ffffff',
        fontSize:20,
        fontWeight:'700'
    },
    
    btn2:{
      
       color:'#0A2339',
       fontSize:20,
     

    }
    
})

export default Language;