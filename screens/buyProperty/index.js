import React from 'react';
import { View,Text,Image,StyleSheet,TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, heightPercentageToDP, widthPercentageToDP }  from '../../utility/index';
const buyProperty=({navigation})=>{
    return(
        <View style={styles.container}>
            <View style={styles.cont2}>
            <Image style={styles.img} source = {require('../../assets/images/icon.png')} resizeMode='contain'/> 
        </View>
        <View style={styles.txt}>
             <Text style={styles.txt1}>I would like to</Text>
              <Text style={styles.txt2}>Please select a option to continue</Text>
          </View>

          <View style={styles.plc}>
         
         <View style={styles.bor1}>
            <Text style={styles.btn1}>Buy Property</Text>
        </View>
    
            <View style={styles.bor2}>
            <Text style={styles.btn2}>Rent Property</Text>
         </View> 
         <View style={styles.drct}>
         <View style={styles.bor3}>
            <TouchableOpacity onPress={()=>navigation.navigate('category')}>
            <Text style={styles.btn3}>Skip Now</Text>
            
            </TouchableOpacity>
         </View>
         </View> 
         </View>
        </View>
    )
};
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#e5eff4',
    },
    cont2:{
        marginTop:'25%' 
    },
    img:{
        alignSelf:'center',
        height:180,
        width:180
    },
    txt1:{
        marginTop:20,
        textAlign:'center',
        fontFamily:'Poppins-Light',
        fontSize:38,
        fontWeight:'500',
        color:'#333333',
        
        // marginBottom:10,
    },
    txt2:{
      fontSize:21,
      fontWeight:'100'  ,
      fontFamily:'Poppins-Light',
      color:'#777777',
      opacity:0.8

    },
    txt:{
       alignSelf:'center',
       marginBottom:20
    },
 
bor1:{
    borderRadius: 50,
    alignSelf:'center',
    margin:'5%',
    // padding:20,
    justifyContent:'center',
    // width:'90%',
    width:widthPercentageToDP('79%'),
    borderWidth:1,
    alignItems: "center",
    backgroundColor:'#0A2339',
    height:heightPercentageToDP('7%')
},
bor2:{
    borderRadius: 50,
    borderWidth:1,
    alignItems: "center",
    alignSelf:'center',
//   width:"90%",
//   marginLeft:'4%',
//   padding:15,
   justifyContent:'center',
    borderColor:'#0A2339',
    width:widthPercentageToDP('79%'),
    height:heightPercentageToDP('7%')
},

    btn1:{
        color:'#ffffff',
        fontSize:20,
        // fontWeight:'700'
        fontFamily:'Poppins-Regular'
    },
    
    btn2:{
      
       color:'#0A2339',
       fontSize:20,
       

    },
    bor3:{
        margin:20
            
    },
        btn3:{
        
       fontSize:20,
       color:'#333333',
       textAlign:'center',
       fontFamily:'Poppins-Regular'
    },
    icon:{
        height:hp(1.8),
        width:wp(2.05),
        left:95,
        bottom:26
        
    },
    drct:{
        flexDirection:'row',
        justifyContent:'center'
    }   
    

})
export default buyProperty;