import { Dimensions } from "react-native";
const { width, height } = Dimensions.get('window');
export const COLORS = {
    primary: "white",
    dots: '#0A2339',
    // #0A2339
    secondDots: 'gray',
    black: "#171717",
    white: "#FFFFFF",
    background: "white"
}

export const SIZES = {
    base: 10,
    width,
    height
}
const theme = { COLORS, SIZES };
export default theme;