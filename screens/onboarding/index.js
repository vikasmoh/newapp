import React, { useRef, useState, useEffect } from 'react';
import { View, Text, StatusBar, SafeAreaView, TouchableOpacity, FlatList, ImageBackground, Image, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, heightPercentageToDP, widthPercentageToDP }  from '../../utility/index';
import { COLORS, SIZES } from '../constant/index';
import data from '../constant/data';
import { template } from '@babel/core';
import * as Utility from '../../utility/index';
const Onboarding = ({ navigation }) => {
    const flatlistRef = useRef();
    const [currentPage, setCurrentPage] = useState(0);
    const [viewableItems, setViewableItems] = useState([])
    const handleViewableItemsChanged = useRef(({ viewableItems }) => {
        setViewableItems(viewableItems)
    })
    useEffect(() => {
        if (!viewableItems[0] || currentPage === viewableItems[0].index)
            return;
        setCurrentPage(viewableItems[0].index)

    }, [viewableItems])



    const renderFlatlistItem = ({ item }) => {
        return (
            <View style={{
                width: SIZES.width,
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                top:10
            }}>
                <View style={{
                    top:30,
                    // marginTop:10
                }}>

                    <View style={{height:'50%',alignItems:'center'}}>
                    <ImageBackground
                        source={item.img}
                        style={{width:435,height:469}}
                        // resizeMode='stretch'
                    />
                    </View>
                </View>
                <View style={{height:'60%',width:'100%'}}>
                <ImageBackground source={require('../../assets/images/onbrdimg2.png')} style={{width:'100%',marginTop:'-15.5%'}}>
              
                <View style={{ marginVertical: SIZES.base * 20 }}>
                    <Text style={{ fontSize: 24,fontFamily:'Poppins-ExtraLight',  marginLeft:15,color:'#333333',fontWeight:'700',bottom:40 }}>
                        {item.title}
                    </Text>
                    <Text style={{ fontSize: 18, color:'#484C76',fontFamily:'Poppins-Medium',marginLeft:15,fontWeight:'500'}}>{item.subtitle}</Text>
                    <Text style={{
                        fontSize: 20,
                        marginLeft:15,
                        bottom:50,
                        lineHeight: 28,
                        color:'#555555',
                    }}>
                        {item.description}
                    </Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center',alignSelf:'center' }}>
                        {
                            // No. of dots
                            [...Array(data.length)].map((_, index) => (
                                <View
                                    key={index}
                                    style={{

                                        width: index==currentPage?26:10,
                                        height:index==currentPage? 12:11,
                                        borderRadius: 10,
                                        backgroundColor: index == currentPage
                                            ? COLORS.dots
                                            : COLORS.secondDots,
                                        // marginRight: 4,
                                        marginLeft:6
                                       
                                        
                                    }} ></View>
                            ))
                        }
                    </View>
                   <View style={{  flexDirection:'row',justifyContent:'center'}}>
                    <View style={{margin:30}}>
                    <TouchableOpacity  onPress={()=>navigation.navigate('Login')}>
                        <Text style={{fontSize:20, color:'#333333',textAlign:'center',fontFamily:'Poppins-Regular'}}>Skip</Text>
                        <Image style={{ height:hp(1.8),width:wp(2.05),left:45,bottom:26}} source={require('../../assets/images/next.png')}/>
                        </TouchableOpacity>
                    </View>
                    </View>
                </View>
              
                </ImageBackground>
                </View>

            </View>
        )
    }

    return (
        <View style={{
            flex: 1,
            backgroundColor: COLORS.background,
            // justifyContent: 'center'
        }}>
            <StatusBar barStyle="dark-content" backgroundColor={COLORS.background} />

          
            <FlatList
                data={data}
                pagingEnabled
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => item._id}
                renderItem={renderFlatlistItem}

                ref={flatlistRef}
                onViewableItemsChanged={handleViewableItemsChanged.current}
                viewabilityConfig={{ viewAreaCoveragePercentThreshold: 100 }}
                initialNumToRender={1}
                extraData={SIZES.width}
            />
         
        </View>
    )
}

export default Onboarding