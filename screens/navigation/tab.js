import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Alert, Image, StyleSheet} from 'react-native';
import {MainStackNavigator, ContactStackNavigator} from './StackNavigator';
// import AllListing from "../btmnav/AllListing";
import EditProfile from "../btmnav/EditProfile";
import HomeScreen from "../locationScreen";
import PostListing from "../btmnav/PostListing";
// import Alisting from '../btmnav/Alisting';
import Alisting from '../btmnav/Alisting';
import ShortListed from "../btmnav/ShortListed";
// import AlLst from '../btmnav/AlLst';
const Tab = createBottomTabNavigator();
const BottomTabNavigator = () => {
  return (
    <Tab.Navigator screenOptions={{headerShown:false}}    tabBarOptions={{
        activeTintColor: '#fff',
        inactiveTintColor: 'lightgray',
        activeBackgroundColor: '#ffff',
        inactiveBackgroundColor: '#ffff',
        
    
        
            style: {
                  backgroundColor: '#CE4418',
                  paddingBottom: 10,
                  borderTopRightRadius:100,//add border top right radius
                borderTopLeftRadius:20,//add border top left radius
                // paddingVertical:3
                
            }
     }}  >
      <Tab.Screen
        name="Home"
        component={HomeScreen}
    
        options={({route}) => ({
            tabBarStyle:{height:80},
          tabBarIcon: ({focused}) => (
            <Image
              source={require('../../assets/images/Homebr.png')}
              style={focused ? styles.selectedTabImg : styles.tabImg}
            />
          ),
          
          
        })}
      />
      <Tab.Screen
        name="Alist"
        component={Alisting}

        options={({route}) => ({
            tabBarStyle:{height:80},
          tabBarIcon: ({focused}) => (
            <Image
            source={require('../../assets/images/btttmbbb.png')}
              style={focused ? styles.selectedTabImg2 : styles.tabImg2}
            />
          ),
        })}
      />
      <Tab.Screen
        name="PostListing"
        component={PostListing}
        options={({route}) => ({
            tabBarStyle:{height:80},
            
          tabBarIcon: ({focused}) => (
            <Image
            source={require('../../assets/images/plsbr.png')}
              style={focused ? styles.selectedTabImg5 : styles.tabImg5}
            />
          ),
        })}
      />
      <Tab.Screen
        name="ShortlistedList"
        component={ShortListed}
        options={({route}) => ({
            tabBarStyle:{height:80},
          tabBarIcon: ({focused}) => (
            <Image
            source={require('../../assets/images/nwblkhrt.png')}
              style={focused ? styles.selectedTabImg3 : styles.tabImg3}
            />
          ),
        })}
      />
      <Tab.Screen
        name="Contactus"
        component={EditProfile}
        options={({route}) => ({
            tabBarStyle:{height:80},
          tabBarIcon: ({focused}) => (
            <Image
            source={require('../../assets/images/btmbr1.png')}
              style={focused ? styles.selectedTabImg4 : styles.tabImg3}
            />
          ),
        })}
      />
    </Tab.Navigator>
  );
};

export default BottomTabNavigator;

const styles = StyleSheet.create({
  tabImg: {
    alignSelf: 'center',
    // tintColor: 'grey',
    
  },
  selectedTabImg: {
    alignSelf: 'center',
    // tintColor: 'red',
  },
  tabImg2: {
    alignSelf: 'center',
    resizeMode:'center',
    // tintColor: 'grey',
    height: 30,
    width: 30,
  },
  selectedTabImg2: {
    alignSelf: 'center',
    // tintColor: '#0097F1',
    // height: 50,
    // width: 40,
  },
  tabImg3: {
    alignSelf: 'center',
    // tintColor: 'grey',
    resizeMode:'center',
    // height: 33,
    // width: 33
  },
  selectedTabImg3: {
    alignSelf: 'center',
    // tintColor: '#0097F1',
    // height: 25,
    // width: 25,
  },
  tabImg5: {
    alignSelf: 'center',
    // tintColor: 'grey',
    resizeMode:'center',
    // height: 33,
    // width: 33
  },
  selectedTabImg5: {
    alignSelf: 'center',
    // tintColor: '#0097F1',
 
    height: 40,
    width: 25,
  },
  tabImg4: {
    alignSelf: 'center',
    // tintColor: 'grey',
    resizeMode:'center',
    height: 33,
    width: 33
  },
  selectedTabImg4: {
    alignSelf: 'center',
    // tintColor: '#0097F1',
 
    height: 40,
    width: 25,
  },
});

