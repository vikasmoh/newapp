import React from 'react';
// import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import selectLanguage from '../mainscreen/selectLanguage';
import Splash from '../mainscreen/splashScreen';
import FilterScreen from '../FilterScreen';

// import Onboarding from '../onboarding/index';
import OnboardingScreen from '../onboarding';
import verificationScreen from '../verificationScreen';
import Login from '../Login/index';
import accountScreen from '../accountScreen';
import loginScreen from '../mainscreen/loginScreen';
import Register from '../mainscreen/registerscreen';
import uploadimageScreen from '../uploadimageScreen';
import imageScreen from '../imageScreen';
import propertyScreen from '../propertyScreen';
import buyProperty from '../buyProperty';
import categoryScreen from '../categoryScreen';
import locationScreen from '../locationScreen';
import Bottomtab from './tab';
import Img1details from '../Img1details';
// import Imagedetails from '../Imagedetails';
// import ListingProperty from '../Propertylist';
// import LandProperty from '../LandProperty';
// import Propertylist from '../Propertylist';
const Stack = createNativeStackNavigator();
function App() {
  return (
    // <NavigationContainer>
      <Stack.Navigator initialRouteName='Splash'>  
     <Stack.Screen name="Splash" 
        component={Splash} options={{headerShown:false}} /> 
        <Stack.Screen name="selectLanguage" 
        component={selectLanguage} options={{headerShown:false}} />
        <Stack.Screen name="OnboardingScreen" 
        component={OnboardingScreen} options={{headerShown:false}} />
          <Stack.Screen name="Login" 
        component={Login} options={{headerShown:false}} />
            <Stack.Screen name="loginScreen" 
        component={loginScreen} options={{headerShown:false}} />
            <Stack.Screen name="verificationScreen" 
        component={verificationScreen} options={{headerShown:false}} />
            <Stack.Screen name="accountScreen" 
        component={accountScreen} options={{headerShown:false}} />
        <Stack.Screen name="register" 
        component={Register} options={{headerShown:false}} />
        <Stack.Screen name="uploadimage" 
        component={uploadimageScreen} options={{headerShown:false}} />
        <Stack.Screen name="image" 
        component={imageScreen} options={{headerShown:false}} />
        <Stack.Screen name="property" 
        component={propertyScreen} options={{headerShown:false}} />
        <Stack.Screen name="buyProperty" 
        component={buyProperty} options={{headerShown:false}} />
        <Stack.Screen name="category" 
        component={categoryScreen} options={{headerShown:false}} />
        <Stack.Screen name="location" 
        component={locationScreen} options={{headerShown:false}} />
        <Stack.Screen name="fltrs" 
        component={FilterScreen} options={{headerShown:false,
        }} />
        <Stack.Screen name="imgd1"
         component={Img1details} options={{headerShown:false}}/>
        {/* <Stack.Screen name='store'
          component={n} options={{headerShown:false}}
        /> */}

        {/* <Stack.Screen name='lst1'
          component={Propertylist} options={{headerShown:false}}
        />
        <Stack.Screen name='lst2'
          component={LandProperty} options={{headerShown:false}}
        /> */}
        {/* <Stack.Screen name='third'
          component={ThirdScreen} options={{headerShown:false}}
        /> */}

        <Stack.Screen name="Bottomatb" component={Bottomtab} options={{headerShown:false}}/>
        {/* <Stack.Screen name="Drawertb" component={Drawertab} options={{headerShown:false}}/> */}
        
      </Stack.Navigator>
    // </NavigationContainer>
  );
}
export default App;