import React,{useState} from 'react';
import { View,Image,StyleSheet,Text, TextInput,TouchableOpacity } from 'react-native';
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, heightPercentageToDP, widthPercentageToDP } from '../../utility/index';
const verificationScreen =({navigation})=>{
const CELL_COUNT = 4;
const [value, setValue] = useState('');
        const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });
    return(
        <View style={styles.container}>
           <View style={{margin:'10%',alignSelf:'center'}}>
                <Image style={styles.appicon} source={require('../../assets/images/appicon.png')} resizeMode="contain"></Image>
            </View>
             <View style={styles.vw1}>
                <Text style={styles.txt1}>Verification Code</Text>
                <Text style={styles.txt2}>Enter OTP code sent to your number</Text>
                <View>
                <CodeField
                     ref={ref}
                     {...props}
//                     // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
                    value={value}
                    onChangeText={setValue}
                     cellCount={CELL_COUNT}
                     rootStyle={styles.codeFieldRoot}
                     keyboardType="number-pad"
                     textContentType="oneTimeCode"
                     renderCell={({ index, symbol, isFocused }) => (
                         <View style={{margin:widthPercentageToDP('1%')}} key={index}>
                             <Text
                                 style={[styles.cell, isFocused && styles.focusCell]}
                                 onLayout={getCellOnLayoutHandler(index)}>
                                 {symbol || (isFocused ? <Cursor /> : null)}
                             </Text>
                         </View>
                     )}
                 />
                </View>
            </View>


            <View style={styles.lgn}>
                    <Text style={{color:'white',alignSelf:'center',fontSize:20}}>Submit</Text>
                </View>


                <View style={{flexDirection:'row',alignSelf:'center',marginTop:20}}>
                
                <Text style={styles.txt}>Didn’t receive an OTP? </Text>
                <TouchableOpacity onPress={()=>navigation.navigate('uploadimage')}>
                <Text style={styles.txt3}>Resend OTP</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
};
const styles = StyleSheet.create({
    root: { flex: 1,  },
    title: { textAlign: 'center', 
    fontSize: 18,
     fontWeight: '500', 
     color: '#484C76' },
    codeFieldRoot: { 
        marginTop: 20
     },
     appicon:{
        height:200,width:200,
        // height:'45%',
        // resizeMode:'center',
        // height:'35%',
        // top:20,
        
    },
    
    cell: {
      
        width: 65,
        height: 65,
        lineHeight: 65,
        fontSize: 24,
        borderWidth: 1,
        borderColor: '#00000030',
        textAlign: 'center',
        borderRadius: 6,
        color: '#484C76'
    },
    focusCell: {
        borderColor: '#000',
        borderRadius: 6,
        color: '#484C76'
    }, 
    container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    bottom:50,
    // marginTop:30,
    backgroundColor:'#e5eff4'
    
    },
    img:{
        marginTop:20,
        alignSelf:'center',
    },
    vw1:{
        marginTop:20
        // alignSelf:'center',
    },
    txt1:{
     color:'#333333',
     fontSize:30,textAlign:'center'
    },
    txt2:{
        // alignSelf:'center',
        fontSize:20,
        textAlign:'center',
        color:'#777777',
    },
    txt3:{
        fontSize:16,
        fontWeight:'700',
        color:'#0A2339',
        alignSelf:'center',
        color:'#487b92',
    },
    box1:{
        borderWidth:1,
        borderColor:'#0A2339',
        borderRadius:7,
        width:70,
        height:70
    },
    box2:{
        borderWidth:1, 
        borderColor:'#0A2339',
        borderRadius:7,
        width:70,
        height:70
    },
    box3:{
        borderWidth:1,
        borderColor:'#0A2339',
        borderRadius:7,
        width:50,
        width:70,
        height:70
    },
    box4:{
        borderWidth:1,
        borderColor:'#0A2339',
        borderRadius:7,
        width:50,
        width:70,
        height:70
    },
    lgn:{
        backgroundColor:'#0A2339',
        width:'90%',
        padding:20,
        borderRadius:50,
        // marginLeft:'6%',
        marginTop:20,
    },
    txt:{
        fontSize:18
    },
    txt3:{
        fontSize:18,
        color:'#1d5b78',
        fontWeight:'bold'
    }
})
export default verificationScreen;

