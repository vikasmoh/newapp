import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, heightPercentageToDP, widthPercentageToDP }
    from '../../utility/index';
const imageScreen = ({ navigation }) => {
    return (
        <View style={styles.mainview}>
            {/* <View style={{margin:widthPercentageToDP('1%')}} */}
            <View style={styles.vw2}>
                <Text style={styles.txt0}>Profile Image</Text>
            </View>
            <View style={styles.imgvw}>
                <Image style={styles.img} source={require('../../assets/images/img1.png')} />
            </View>
            <View style={styles.img2vw}>
                <Image style={styles.img001} source={require('../../assets/images/del.png')} />
                <Text style={styles.txt01}>Delete Image</Text>
            </View>
            <View style={styles.bor1}>
                <Text style={styles.btn1}>Change Image</Text>
            </View>
            <View style={styles.bor2}>
                <Text style={styles.btn2}>Next</Text>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('property')}>
                <Text style={{ top: 20 }}>next</Text>
            </TouchableOpacity>
        </View>
    )
};
const styles = StyleSheet.create({
    mainview: {
        alignItems: 'center',
        justifyContent: 'center',
        // marginBottom:40,
        // margintop:100
        backgroundColor: '#e5eff4',
        flex: 1,



    },
    imgvw: {
        marginBottom: 20
    },
    txt0: {
        marginBottom: 20,
        fontSize: 33,
        color: '#333333',
        fontFamily: 'Poppins-Regular',
        // fontWeight:'bold'

        // fontWeight:'700'
    },
    vw2: {
        // marginTop:20
    },
    img: {
        width: widthPercentageToDP('50%'),
        height: heightPercentageToDP('23%'),

        // marginBottom:40
    },
    bor1: {
        borderRadius: 50,
        margin: '5%',
        padding: 15,
        width: '75%',
        borderWidth: 1,
        alignItems: "center",
        backgroundColor: '#ffffff',
        borderColor: '#0A2339'
    },
    btn1: {
        color: '#0A2339',
        fontSize: 20,
        fontFamily: 'Poppins-Regular'
    },
    bor2: {
        backgroundColor: '#0A2339',
        borderRadius: 50,
        // borderWidth:1,
        alignItems: "center",
        width: "75%",
        marginLeft: '5%',
        padding: 15,
        borderColor: '#0A2339'
    },
    btn2: {
        color: '#ffffff',
        fontSize: 20,
        fontFamily: 'Poppins-Regular'
    },
    vew00: {
        top: 20
    },
    txt01: {
        fontSize: 18,
        fontFamily: 'Poppins-Regular'
    },
    img2vw: {
        flexDirection: 'row',
        alignItems: 'center'

    },
    img001: {
        height: 25,
        width: 20,
        right: 10
    },
    txt01: {
        color: '#004566',
        fontFamily: 'Poppins-Regular',
        fontSize: 21,
        top: 4
    }
})
export default imageScreen;