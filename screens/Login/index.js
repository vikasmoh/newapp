import React from 'react';
import { Text, View,StyleSheet,Image, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, heightPercentageToDP, widthPercentageToDP } from '../../utility/index';

// import {widthPercentageToDP as wp} from '../../utility';
// import { heightPercentageToDP as hp} from '../../utility';
const Login = ({navigation}) => {
    return (
        <View style={styles.container}>
            <View style={{alignSelf:'center',margin:'6%'}}>
                <Image source={require('../../assets/images/onbrd4.png')}></Image>
            </View>
            <View style={{margin:'6%'}}>
                <Text style={{fontSize:25,fontWeight:'400',color:'#333333'}}>We make spotlight</Text>
                <Text style={{fontSize:33,fontWeight:'300',fontFamily:'Poppins-Bold',color:'#333333'}} >on your property!</Text>
            </View>
            <View style={{flexDirection:'row',margin:'6%',justifyContent:'space-evenly'}}>
                <View style={{backgroundColor:'#0A2339',width:'46%',padding:15,borderRadius:50}}>
                    <Text style={{color:'white',alignSelf:'center',fontSize:21}}>Login</Text>
                </View>
                <View style={{width:'46%',padding:15,borderRadius:50,borderWidth:1,borderColor:'#0A2339'}}>
                    <Text style={{alignSelf:"center",fontSize:21}}> Register</Text>
                </View>
            </View>
            <View style={styles.drct}>
            <View style={{alignItems:'center'}}>
            <TouchableOpacity onPress={()=>navigation.navigate('Bottomatb')}>
                <Text style={styles.btn3}>Skip Now </Text>
                <Image style={styles.icon} source={require('../../assets/images/next.png')}/>
                </TouchableOpacity>
            </View>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'#e5eff4',
       
        flex:1,
    },
    btn3:{
        
        fontSize:20,
        color:'#333333',
        textAlign:'center',
        fontFamily:'Poppins-Regular'
     },
    icon:{
        height:hp(1.8),
        width:wp(2.05),
        left:95,
        bottom:26
        
    },
    drct:{
        flexDirection:'row',
        justifyContent:'center'
    }
    
})
// const styles = StyleSheet.create({
// cont1:{
//    flex:1, 
//   alignItems:'center',
//   backgroundColor:'#e8f1f5',
// },
// img:{
//     marginTop:40,
// },
// txt1:{
//     fontSize:30,
//     fontWeight:'500',
//     color:'#333333',

// },
// txt2:{
//    color:'#777777',
//    fontSize:20,
// //    marginBottom:40
// },
// input:{
 
// },
// div:{
//    borderWidth:1,
//     width:100,
   
// }
// })
export default Login;
