import React, { useState } from 'react';
import { ScrollView } from 'react-native';
import { Image, View, StyleSheet, Text, FlatList, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../utility";
const Img1details = ({ navigation }) => {
    const [newArray1, setNewArray1] = useState([
        { image: require('../../assets/images/iimmg.png'), items: 'House for sale', price: '$18,00,000', status: 'false' },
        { image: require('../../assets/images/ioas.png'), items: 'House for sale', price: '$28,00,000', status: 'false' },
        { image: require('../../assets/images/iimmg.png'), items: 'House for sale', price: '$20,00,000', status: 'false' }
    ])
    const keyExtractor = (item, index) => item + index;
    const newrender1 = ({ item, index }) => {
        return (
            <View>
                {/* <View style={{flexDirection:'row',width:'90%',borderWidth:1}}> */}
                <View style={{
                    margin: 20, padding: 5, backgroundColor: '#E5E5E5', width: wp('93%'),
                    borderTopLeftRadius: 20, flexDirection: 'row', height: hp('27%'), borderBottomLeftRadius: 20
                }}>
                    <View style={{ position: 'absolute', zIndex: 1, alignSelf: 'flex-end', top: 10, right: '60%' }}>
                        <TouchableOpacity onPress={() => setheart(item)
                        }>
                            {item.status == 'true' ?
                                <Image style={{ resizeMode: 'center' }} source={require('../../assets/images/redhrt.png')}>
                                </Image> :
                                <Image style={{ resizeMode: 'center' }} source={require('../../assets/images/blkhrt.png')}></Image>}
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: wp('40%') }}>
                        <Image source={item.image} style={{ height: hp('26%'), width: wp('40%'), borderBottomLeftRadius: 20, borderTopLeftRadius: 20, marginBottom: 10 }}></Image>
                    </View>
                    <View style={{ justifyContent: 'center' }}>
                        <View style={{ marginLeft: 20 }}>
                            <Text style={{ fontSize: 20, fontWeight: '400', color: '#333333', opacity: 0.7, fontFamily: 'Poppins-Regular' }}>{item.items}</Text>
                            <Text style={{ fontSize: 20, color: '#0A2339', fontFamily: 'Poppins-Medium', fontWeight: '500', opacity: 0.9 }}>{item.price}</Text>
                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View>
                                    <Image style={{ height: 20, width: 15 }} source={require('../../assets/images/lcn2.png')}></Image>
                                </View>
                                <View>
                                    <Text style={{ left: 5, fontSize: 16, color: '#555555', fontFamily: 'Poppins-Regular', fontWeight: '400', opacity: 0.7 }}>Moqadishu, Somalia</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', margin: 10 }}>
                                <View style={{ right: 5 }}>
                                    <Image source={require('../../assets/images/noptn.png')} style={{ height: 22, width: 22 }}></Image>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 17, color: '#333333', fontFamily: 'Poppins-Regular', fontWeight: '600' }}>Property</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View>
                                    <Image style={{ height: 20, width: 20 }} source={require('../../assets/images/nwdbl.png')}></Image>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 17, color: '#333333', fontFamily: 'Poppins-Regular', fontWeight: '600' }}>300 &times; 200(m)</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }



    return (
        <ScrollView style={{ backgroundColor: '#F3F3F3' }}>
            <View>
                <View>
                    <Image style={styles.imageview} source={require('../../assets/images/iikn.png')} />
                </View>
                <View style={{ margin: 10 }}>
                    <Text style={{ fontSize: 20, fontWeight: '400', color: '#333333', opacity: 0.7, fontFamily: 'Poppins-Regular' }}>House for sale</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View>
                        <Image resizeMode='center' source={require('../../assets/images/lcn2.png')} />
                    </View>
                    <View>
                        <Text style={{ left: 5, fontSize: 16, color: '#555555', fontFamily: 'Poppins-Regular', fontWeight: '400', opacity: 0.7 }}>Moqadishu, Somalia</Text>
                    </View>
                </View>
                <View style={{ marginTop: 10 }}>
                    <Text style={{ fontSize: 16, color: '#0A2339', fontFamily: 'Poppins-Medium', fontWeight: '500', opacity: 0.9, }}>12,00,000 - $18,00,000</Text>
                </View>
                <View style={{ flexDirection: 'row', right: 45, justifyContent: 'space-evenly', marginTop: 10 }}>
                    <Image style={{ resizeMode: 'center' }} source={require('../../assets/images/bed.png')} />
                    <Text style={{ lineHeight: 20, fontSize: 16, fontWeight: '400', color: '#333333', fontFamily: 'Poppins-Regular', top: 20, top: 22, right: 35 }}>5</Text>
                    <Image style={{ resizeMode: 'center', width: wp('5%') }} source={require('../../assets/images/shwr.png')} />
                    <Text style={{ lineHeight: 20, fontSize: 16, fontWeight: '400', color: '#333333', fontFamily: 'Poppins-Regular', top: 20, top: 22, right: 35 }}>3</Text>
                    <Image style={{ resizeMode: 'center' }} source={require('../../assets/images/grg.png')} />
                    <Text style={{ lineHeight: 20, fontSize: 16, fontWeight: '400', color: '#333333', fontFamily: 'Poppins-Regular', top: 22, right: 35 }}>1</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', right: '15%', marginTop: 10 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ right: 5 }}>
                            <Image source={require('../../assets/images/noptn.png')} style={{ height: 16, width: wp('3.9%') }}></Image>
                        </View>
                        <View >
                            <Text style={{ fontSize: 14, color: '#333333', fontFamily: 'Poppins-Regular', fontWeight: '400', lineHeight: 21, }}>Property</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', }}>
                        <View>
                            <Image style={{ height: 20, width: 20 }} source={require('../../assets/images/nwdbl.png')} style={{ height: 16, width: wp('3.9%') }}></Image>
                        </View>
                        <View>
                            <Text style={{ fontSize: 17, color: '#333333', fontFamily: 'Poppins-Regular', fontWeight: '600' }}>300 &times; 200(m)</Text>
                        </View>
                    </View>


                    <View style={{ marginTop: 10 }}>
                        <Text style={{ color: '#333333', fontSize: 16, lineHeight: 24, fontFamily: 'Poppins-Regular', fontWeight: '500' }}>Description</Text>
                        <Text style={{ fontSize: 14, lineHeight: 26, fontWeight: '400', color: '#333333', fontFamily: 'Poppins-Regulr' }}>Lorem ipsum dolor sit amet, consectetur {'\n'}adipiscing elit, sed do eiusmod incididunt ut{'\n'}labore et dolore magna aliqua.
                            Ut enim ad{'\n'}minim veniam quis ullamco laboris nisi ut{'\n'}aliquip ex ea commodo consequat.</Text>
                    </View>

                </View>
                <View style={{ marginLeft: 10 }}>
                    <Text style={{ fontSize: 16, fontWeight: '500', lineHeight: 24, color: '#333333', fontFamily: 'Poppins-Regular' }}>Amenities</Text>
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 10, justifyContent: 'space-around' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image style={{ resizeMode: 'center', height: hp('2%'), width: wp('2%'), top: 5, right: 3 }} source={require('../../assets/images/dot.png')} />
                        <Text style={{ fontSize: 16, lineHeight: 26, fontWeight: '400', color: '#333333' }}>Barbeque</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Image style={{ resizeMode: 'center', height: hp('2%'), width: wp('2%'), top: 5, right: 3 }} source={require('../../assets/images/dot.png')} />
                        <Text style={{ fontSize: 16, lineHeight: 26, fontWeight: '400', color: '#333333' }}>Barbeque</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 10, justifyContent: 'space-around' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image style={{ resizeMode: 'center', height: hp('2%'), width: wp('2%'), top: 5, right: 3 }} source={require('../../assets/images/dot.png')} />
                        <Text style={{ fontSize: 16, lineHeight: 26, fontWeight: '400', color: '#333333' }}>Barbeque</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Image style={{ resizeMode: 'center', height: hp('2%'), width: wp('2%'), top: 5, right: 3 }} source={require('../../assets/images/dot.png')} />
                        <Text style={{ fontSize: 16, lineHeight: 26, fontWeight: '400', color: '#333333' }}>Barbeque</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 10, justifyContent: 'space-around' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image style={{ resizeMode: 'center', height: hp('2%'), width: wp('2%'), top: 5, right: 3 }} source={require('../../assets/images/dot.png')} />
                        <Text style={{ fontSize: 16, lineHeight: 26, fontWeight: '400', color: '#333333' }}>Barbeque</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Image style={{ resizeMode: 'center', height: hp('2%'), width: wp('2%'), top: 5, right: 3 }} source={require('../../assets/images/dot.png')} />
                        <Text style={{ fontSize: 16, lineHeight: 26, fontWeight: '400', color: '#333333' }}>Barbeque</Text>
                    </View>
                </View>

                {/* <View style={{borderRadius:10,height:hp('15%'),backgroundColor:'#E5E5E5',width:wp('90%'),marginLeft:20,top:20}}>
                        <View style={{flexDirection:'row',}}>
                            <Text style={{borderWidth:1}}>View Profile</Text>
                            <Text style={{borderWidth:1}}>Contact Details</Text>
                        </View>
                    </View> */}
                <View style={{
                    height: hp('15%'), backgroundColor: '#E5E5E5', width: wp('96%'), margin: 10, borderRadius: 10,
                }}>
                    <View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                            <View>
                                <Image style={{ height: hp('5%'), width: wp('12%'), resizeMode: 'center' }} source={require('../../assets/images/lg.png')}></Image>
                            </View>
                            <View style={{}}>
                                <View>
                                    <Text style={{ fontSize: 16, color: '#004566', fontFamily: 'Poppins-Regular', fontWeight: '400', opacity: 0.8, right: 25 }}>Ahmed Sayed</Text>
                                </View>
                                <View style={{ right: 27, }}>
                                    <View style={{ flexDirection: 'row', left: 20 }}>
                                        <View>
                                            <Image source={require('../../assets/images/start.png')}></Image>
                                        </View>
                                        <View>
                                            <Image source={require('../../assets/images/staruncheck.png')}></Image>
                                        </View>
                                        <View>
                                            <Image source={require('../../assets/images/staruncheck.png')}></Image>
                                        </View>
                                        <View>
                                            <Image source={require('../../assets/images/staruncheck.png')}></Image>
                                        </View>
                                        <View>
                                            <Image source={require('../../assets/images/staruncheck.png')}></Image>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ bottom: 9, right: 14 }}>
                                    <Image style={{ width: wp('5%'), height: hp('5%'), resizeMode: 'center' }} source={require('../../assets/images/nclndr.png')}></Image>
                                </View>
                                <View style={{}}>
                                    <Text style={{ fontSize: 16, color: '#555555', fontFamily: 'Poppins-Regular', fontWeight: '400', opacity: 0.8, right: 10, top: 4 }}>5 days ago</Text>
                                </View>
                            </View>
                        </View>

                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                        <View style={{ borderWidth: 1, borderRadius: 30, height: 36, width: 145, justifyContent: 'center', borderColor: '#0A2339' }}>
                            <Text style={{ fontSize: 15, lineHeight: 30, fontWeight: '400', fontFamily: 'Poppins-Regular', textAlign: 'center', color: '#0A2339' }}>View Details</Text>
                        </View>
                        <View style={{ borderWidth: 1, borderRadius: 30, height: 36, width: 145, backgroundColor: '#0A2339', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 15, lineHeight: 30, color: '#FFFFFF', fontWeight: '400', fontFamily: 'Poppins-Regular', textAlign: 'center' }}>Contact Details</Text>
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10 }}>
                    <Text style={{ fontSize: 18, lineHeight: 30, fontWeight: '500', fontFamily: 'Poppins-Regular', color: '#333333' }}>Similar Listings</Text>
                    <Text style={{ fontSize: 14, lineHeight: 30, fontWeight: '400', fontFamily: 'Poppins-Regular', color: '#004566' }}>View all</Text>
                </View>
                <View>

                    <FlatList
                        data={newArray1}
                        renderItem={newrender1}
                        keyExtractor={keyExtractor}
                        // nestedScrollEnabled={true}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                    // numColumns={newArray.length}
                    >
                    </FlatList>

                </View>
            </View>
        </ScrollView>
    )
};
const styles = StyleSheet.create({
    imageview: {
        height: 284.67,
        width: wp('100%')
    }
})
export default Img1details;